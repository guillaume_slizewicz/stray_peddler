# The devogram

### Description

The devogram is a mobile broadcasting robot,a political amplifier for urban environment. It can track people, follow them and "talk" to them, broadcasting sound or sentences prepared in advance. 

The devogram can detect objects and humans in the street thanks to an object detection algorithm (namely mobilenet v2 with the COCO dataset and labels). In this version, the sentences it reads comes from what an OCR algorithm could decypher from images of a public street, "le pietonnier", in Brussels.

Introducing Video:
https://youtu.be/kB1oQwy2_7s



![animal](./img/animal.gif)

![children_3](./img/children_3.gif)

### ![shoot](./img/shoot.gif)



### Hardware

The devogram is a robot containing one arduino board with a motor shield and four actuators. In its original form it ran with a raspberry pi, a pi camera, one lithium battery and a speaker.
Latest version of the devogram are running on a google coral board, resulting in faster interactions and movements.

![devo_2](./img/devo_2.jpg)

![devo_3](./img/devo_3.jpg)

![devo_4](./img/devo_4.jpg)



### Software

The devogram scripts are written in Python and Arduino C++ , you can find them here.



# 2d iteration

For the second iteration we chose to used pre-recorded audio snippet with a more recognisable voice able to compete with urban noises (construction works, car, urban musicians) and a amplifying structure.


![devo_21](./img/devo_21.JPG)

![devo_22](./img/devo_22.JPG)

![devo_23](./img/devo_23.JPG)

![devo_24](./img/devo_24.JPG)

![devo_25](./img/devo_25.JPG)

![devo_26](./img/devo_26.JPG)

![devo_27](./img/devo_27.JPG)



### Acknowledgment

The project was created with the help of [this tutorial](https://github.com/EdjeElectronics/TensorFlow-Object-Detection-on-the-Raspberry-Pi)

### About Urban Species

We are Urban Species, a research group based in Brussels focusing on citizen participation. We are a multi-disciplinary team at the crossroad of anthropology, urban planning, design and sociology. Urban Species brings together researchers from ULB and Luca School of Arts. As part of its prototyping strategies and experiments, Urban Species collaborates with developers, graphic designers, but also citizens, associations, public authorities and a whole series of species, human or not, including electronic and digital fauna.

Urban Species is currently engaged in two projects, P-lab and SUCIB, which are part of Innoviris' Anticipate program, under the theme "Societal participation and citizenship".

Find more at: https://www.instagram.com/especesurbaines/



