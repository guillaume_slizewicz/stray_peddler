######## Picamera Object Detection Using Tensorflow Classifier #########
#
# Author: Evan Juras, Guillaume Slizewicz
# Date: 4/15/18
# Modification: 10/24/18
# Description: 
# This program uses a TensorFlow classifier to perform object detection.
# It loads the classifier uses it to perform object detection on a Picamera feed.
# It draws boxes and scores around the objects of interest in each frame from
# the Picamera. It also can be used with a webcam by adding "--usbcam"
# when executing this script from the terminal.

## Some of the code is copied from Google's example at
## https://github.com/tensorflow/models/blob/master/research/object_detection/object_detection_tutorial.ipynb

## and some is copied from Dat Tran's example at
## https://github.com/datitran/object_detector_app/blob/master/object_detection_app.py

## but I changed it to make it more understandable to me.

import sys
sys.path.append('/home/pi/.local/lib/python3.5/site-packages')
sys.path.append('/home/pi/.local/lib/python3.5/dist-packages')
sys.path.append('/usr/local/lib/python3.5/site-packages')
sys.path.append('/usr/local/lib/python3.5/dist-packages')

# Import packages
import os
import numpy as np
import cv2
from picamera.array import PiRGBArray
from picamera import PiCamera
import tensorflow as tf
import argparse
import serial
import time
from os import system
import shlex
import subprocess
import threading
from pathlib import Path


#############TEXT TO READ #################
f_adressed = "/home/pi/Documents/sounds/text_bourse"
n= 0
list_file = []
# f_general= open("/home/pi/tensorflow1/models/research/object_detection/bourse_general.txt", "r",encoding='utf-8')

for root, dirs, files in os.walk(f_adressed):
    for file in files:
        if file.endswith('.wav'):
            list_file.append(file)

print(list_file)

def speaker(list_to_play):
    text_to_play=f_adressed+"/"+list_to_play[n]
    print(text_to_play)
    os.system('aplay {}'.format(text_to_play))
    global n
    n+=1
    if n==len(list_to_play):
        n=0
        print(n)
        # do stuff

speaker_thread = threading.Thread(target=speaker, args=[list_file])


############ /ENDOF TEXT TO READ #################


############# OBJECT DETECTION #################

ser = serial.Serial('/dev/ttyACM0',9600)


# Set up camera constants

#IM_WIDTH = 1280
#IM_HEIGHT = 720
IM_WIDTH = 640    #Use smaller resolution for
IM_HEIGHT = 480   #slightly faster framerate

# Select camera type (if user enters --usbcam when calling this script,
# a USB webcam will be used)

camera_type = 'picamera'
parser = argparse.ArgumentParser()
parser.add_argument('--usbcam', help='Use a USB webcam instead of picamera',
                    action='store_true')
args = parser.parse_args()
if args.usbcam:
    camera_type = 'usb'

# This is needed since the working directory is the object_detection folder.
sys.path.append('..')

# Import utilites
from utils import label_map_util
from utils import visualization_utils as vis_util

# Name of the directory containing the object detection module we're using
MODEL_NAME = 'ssdlite_mobilenet_v2_coco_2018_05_09'

# Grab path to current working directory
CWD_PATH = os.getcwd()


# Path to frozen detection graph .pb file, which contains the model that is used
# for object detection.
PATH_TO_CKPT = os.path.join(CWD_PATH,MODEL_NAME,'frozen_inference_graph.pb')

# Path to label map file
PATH_TO_LABELS = os.path.join(CWD_PATH,'data','mscoco_label_map.pbtxt')

# Number of classes the object detector can identify
NUM_CLASSES = 90

## Load the label map.
# Label maps map indices to category names, so that when the convolution
# network predicts `5`, we know that this corresponds to `airplane`.
# Here we use internal utility functions, but anything that returns a
# dictionary mapping integers to appropriate string labels would be fine
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
category_index = label_map_util.create_category_index(categories)

# Load the Tensorflow model into memory.
detection_graph = tf.Graph()
with detection_graph.as_default():
    od_graph_def = tf.GraphDef()
    with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')

    sess = tf.Session(graph=detection_graph)


# Define input and output tensors (i.e. data) for the object detection classifier

# Input tensor is the image
image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

# Output tensors are the detection boxes, scores, and classes
# Each box represents a part of the image where a particular object was detected
detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

# Each score represents level of confidence for each of the objects.
# The score is shown on the result image, together with the class label.
detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')

# Number of objects detected
num_detections = detection_graph.get_tensor_by_name('num_detections:0')

# Initialize frame rate calculation
frame_rate_calc = 1
freq = cv2.getTickFrequency()
font = cv2.FONT_HERSHEY_SIMPLEX

# Initialize camera and perform object detection.
# The camera has to be set up and used differently depending on if it's a
# Picamera or USB webcam.



################
### Picamera ###
################

if camera_type == 'picamera':
    # Initialize Picamera and grab reference to the raw capture
    camera = PiCamera()
    camera.resolution = (IM_WIDTH,IM_HEIGHT)
    camera.framerate = 10
    rawCapture = PiRGBArray(camera, size=(IM_WIDTH,IM_HEIGHT))
    rawCapture.truncate(0)

    for frame1 in camera.capture_continuous(rawCapture, format="bgr",use_video_port=True):

        t1 = cv2.getTickCount()
        
        # Acquire frame and expand frame dimensions to have shape: [1, None, None, 3]
        # i.e. a single-column array, where each item in the column has the pixel RGB value

        frame = frame1.array
        frame.setflags(write=1)
        frame_expanded = np.expand_dims(frame, axis=0)

        # Perform the actual detection by running the model with the image as input

        (boxes, scores, classes, num) = sess.run(
            [detection_boxes, detection_scores, detection_classes, num_detections],
            feed_dict={image_tensor: frame_expanded})

        # Draw the results of the detection (aka 'visulaize the results')

        vis_util.visualize_boxes_and_labels_on_image_array(
            frame,
            np.squeeze(boxes),
            np.squeeze(classes).astype(np.int32),
            np.squeeze(scores),
            category_index,
            use_normalized_coordinates=True,
            line_thickness=6,
            min_score_thresh=0.40)

        


#####################                                      #####################
#####################   Code for detecting person and      #####################
#####################               defining intervals     #####################
#####################                                       #####################

        # define interval for center, left and right in the display
        middle= 0.5

        for index,value in enumerate(classes[0]): # checking through all values for all classes
            if scores[0,index] > 0.5:             #  getting the ones with a prediction over 50%
                detected= category_index.get(value) # translate the class value which is an int into its name, a string
                if(( detected["name"])== "person"): # condition if the object detected is a person

                    box_left_corner = boxes[0,index,1] 
                    box_right_corner = boxes[0,index, 3]


# give command if it is on the left, on the right or in the middle.
# send command via serial to arduino



##################################                                       ##################################
##################################    Code for moving motors according   ##################################
##################################               to object               ##################################
##################################                                       ##################################

                    if box_right_corner - box_left_corner <0.20 and box_right_corner - box_left_corner >0.10:  #person too small so move forward
                        ser.write(str.encode('f')) 
                        print("move forward") 

                    if  box_right_corner - box_left_corner <0.10:  #person too small so move forward
                            # if not speaker_thread.is_alive():
                            #     speaker_thread = threading.Thread(target=speaker, args= [f_])
                            #     speaker_thread.start()
                                print("f_general")


                    elif box_right_corner - box_left_corner  >0.80: #person too big so move backward
                        ser.write(str.encode('b')) 
                        print("move backward") 

                    elif box_right_corner < middle : #person on the left
                        ser.write(str.encode('l')) 
                        print("move left") 



                    elif box_left_corner > middle : #person on the right
                        ser.write(str.encode('r')) 
                        print("move right")


                    else:
                        ser.write(str.encode('s')) #stops if nothing happens
                        if not speaker_thread.is_alive():
                            speaker_thread = threading.Thread(target=speaker, args= [list_file])
                            speaker_thread.start()
                            print("f_adressed")
                        
                else:
                    ser.write(str.encode("s"))
                    # if not speaker_thread.is_alive():
                    #     speaker_thread = threading.Thread(target=speaker, args=[f_general,3,30])
                    #     speaker_thread.start()
                    #     print("f_general")
                    # time.sleep(0.2)



        if cv2.waitKey(1) == ord('q'):
            break

        rawCapture.truncate(0)

    camera.close()


cv2.destroyAllWindows()

