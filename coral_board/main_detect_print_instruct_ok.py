# Copyright 2019 Google LLC
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     https://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

#####Libraries####


import argparse
import lib.gstreamer as gstreamer
import lib.utils as utils
from edgetpu.detection.engine import DetectionEngine
from edgetpu.classification.engine import ClassificationEngine
import serial


ser = serial.Serial("/dev/ttyACM0", 9600)

####Functions graphics####

def draw_rectangle(draw, coordinates, color, width=1):
    for i in range(width):
        rect_start = (coordinates[0] - i, coordinates[1] - i)
        rect_end = (coordinates[2] + i, coordinates[3] + i)
        draw.rectangle((rect_start, rect_end), outline = color)




#####Functions detect####


def init_engine(model):
    """Returns an Edge TPU classifier for the model"""
    # TODO: Instantiate a ClassificationEngine for the given model
    return DetectionEngine(model)

def input_size(engine):
    """Returns the required input size for the model"""
    # TODO: Return the proper input size for the given model
    _, h, w, _ = engine.get_input_tensor_shape()
    return w, h

def inference_time(engine):
    """Returns the time taken to run inference"""
    # TODO: Return the inference time from the ClassificationEngine
    return engine.get_inference_time()

def detect_image(tensor, engine, labels):
    """Runs inference on the provided input tensor and
    returns an overlay to display the inference results
    """
    # TODO: Run inference on the provided input tensor
    results = engine.DetectWithInputTensor(
        tensor, threshold=0.5, top_k=3)

    return [(i.bounding_box.flatten().tolist(),labels[i.label_id]) for i in results]

# labels[i.label_id], i.score, 


#####Main####


def main(args):
    middle= 0.5

    input_source = "{0}:YUY2:{1}:{2}/1".format(args.source, args.resolution, args.frames)
    print("input ok")
    labels = utils.load_labels(args.labels)
    print("label ok")
    engine = init_engine(args.model)
    print("engine ok")
    inference_size = input_size(engine)
    print("inference ok")
    def frame_callback(tensor, layout, command):
        results = detect_image(tensor, engine, labels)
        # print("results ok")
        time = inference_time(engine)
        if results and time:
            if (results[0][1]== "person"):
                print("person found")
            else:
                print("no person found")

    #####################                                      #####################
    #####################   Code for detecting person and      #####################
    #####################               defining intervals     #####################
    #####################                                       #####################

            # define interval for center, left and right in the display
            

            if (results[0][1]== "person"): # condition if the object detected is a person

                box_left_corner = results[0][0][0]
                box_right_corner = results[0][0][2]


    # give command if it is on the left, on the right or in the middle.
    # send command via serial to arduino


    ##################################                                       ##################################
    ##################################    Code for moving motors according   ##################################
    ##################################               to object               ##################################
    ##################################                                       ##################################

                if box_right_corner - box_left_corner <0.20 and box_right_corner - box_left_corner >0.10:  #person too small so move forward
                    # ser.write(str.encode('f')) 
                    print("move forward") 

                if  box_right_corner - box_left_corner <0.10:  #person too small so move forward
                        # if not speaker_thread.is_alive():
                        #     speaker_thread = threading.Thread(target=speaker, args= [f_])
                        #     speaker_thread.start()
                            print("f_general")


                elif box_right_corner - box_left_corner  >0.80: #person too big so move backward
                    # ser.write(str.encode('b')) 
                    print("move backward") 

                elif box_right_corner < middle : #person on the left
                    ser.write(str.encode('l')) 
                    print("move left") 

                elif box_left_corner > middle : #person on the right
                    ser.write(str.encode('r')) 
                    print("move right")


                else:
                    ser.write(str.encode('s')) #stops if nothing happens
                    # if not speaker_thread.is_alive():
                        # speaker_thread = threading.Thread(target=speaker, args= [list_file])
                        # speaker_thread.start()
                    print("f_adressed")
                            
            else:
                print("ok")
                # ser.write(str.encode("s"))
                        # if not speaker_thread.is_alive():
                        #     speaker_thread = threading.Thread(target=speaker, args=[f_general,3,30])
                        #     speaker_thread.start()
                        #     print("f_general")
                        # time.sleep(0.2)

#####################                                      #####################
#####################   Code for detecting person and      #####################
#####################               defining intervals     #####################
#####################                                       #####################

            # return utils.overlay('Edge TPU Image Classifier', results, time, layout)
            

    gstreamer.run(inference_size, frame_callback,
        source=input_source,
        loop=False,
        display=None
        )






#####Parsing####

if __name__ == '__main__':
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--source',
                    help='camera device (e.g. /dev/video0)',
                    default='/dev/video0')
    parser.add_argument('--resolution',
                    help='camera capture resolution',
                    default='1280x720')
    parser.add_argument('--frames',
                    help='camera capture frame rate',
                    default='30')
    parser.add_argument('--model', required=True,
                    help='.tflite model path')
    parser.add_argument('--labels', required=True,
                    help='label file path')
    args = parser.parse_args()

    main(args)
